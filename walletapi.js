const Btcore = require('bitcore-lib');
//var litecore = require('litecore-lib');
const InsightBitcoin = require('bitcore-explorers').Insight;
//var InsightLitecoin = require('litecoin-explorers').Insight;
const bitcoinfees = require('bitcoinfees-21co');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const mongoDB = 'mongodb://127.0.0.1:27017/forkcoin_db';
const express = require('express');
const app = express();
const cors = require('cors');
var forEach = require('async-foreach').forEach;
const bodyParser = require('body-parser');
const Cryptr = require('cryptr');
var request = require('request');
var nodemailer = require('nodemailer');
var replaceall = require("replaceall");
var dateTime = require('node-datetime');
app.use(express.static('public'));
app.use(cors());
const urlencodedParser = bodyParser.urlencoded({ extended: false });

//connect to database
mongoose.connect(mongoDB, { useMongoClient: true });
const db = mongoose.connection;

// load Users model
var Users = require('./model/users.js');
var UserAddresses = require('./model/UserAddresses.js');
var Transaction = require('./model/transaction.js');

bitcoinNodeUrl = 'https://insight.bitpay.com';
currentServerUrl = 'http://34.219.80.14:3132';
bitNetworkType = 'mainnet';
//firebase Notification
var admin = require("firebase-admin");

var serviceAccount = require("./bitserve-firebase-adminsdk.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bitserve-da531.firebaseio.com"
});
// mail transporter
var transporter = nodemailer.createTransport({
    host: 'res102.asoshared.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'noreply@btc30minutes.com', // generated ethereal user
        pass: '!N123abcJeje', // generated ethereal password
    }
});
//console.log(Btcore.Unit.fromMilis(0.150).toBTC());
let minerFee = Btcore.Unit.fromMilis(0.100).toSatoshis();
let minerFeeBtc = Btcore.Unit.fromMilis(0.150).toBTC().toString();
let totalFee = Btcore.Unit.fromMilis(0.250).toSatoshis();
let totalFeeBtc = Btcore.Unit.fromMilis(0.250).toBTC().toString();


app.get('/', urlencodedParser, function(req, res) {
    res.send({status: "working"});
})

app.get('/checkAddress/:addr', urlencodedParser, function(req, res) {
    var data;
    var address = req.params.addr;
    UserAddresses.find({ 'bitcoin_address': address }, function(err, info) {
        if (err) {
            data = {
                'status': 'err'
            }
            res.status(200).send(data);
        } else {
            if(info.length==1){
                data = {
                    'status': 1,
                    'msg': "Address Verified"
                }
            }else{
                data = {
                    'status': 0,
                    'msg': "Address Not Found"
                }
            }
            res.status(200).send(data);
        }
    })
});

function allDone(notAborted, arr) {
    console.log("done", notAborted, arr);
}
app.post('/getTotalBalance', urlencodedParser, function(req, res) {
    let totalBal = 0;
    let i=0;
    let address="";
    Users.find({}, function(err, users) {
        if (err) return console.error(err);
        console.log("total user - "+users.length)
        users.forEach(async(element,index) => {
            await UserAddresses.find({ 'u_id': element._id }, async function(err, info) {
                if (err) {
                    data = {
                        'status': 'err_addrinfo'
                    }
                    //res.status(200).send(data);
                } else if(info.length>0) {
                    //console.log("addresses", info[0].bitcoin_address);
                    if(info[0].bitcoin_address!="undefined" && info[0].bitcoin_address!=undefined){
                        if(address=="")
                        address = info[0].bitcoin_address
                        else
                        address = address+"|"+info[0].bitcoin_address;
                    }
                    
                    console.log("sno index", index);
                    if(index==(users.length-1) || index%19==0){
                        console.log("index", index);
                        // console.log(users.length, i);
                        console.log("addresses", address);
                        var options = {
                            method: 'GET',
                            url: 'https://blockchain.info/q/addressbalance/' + address,
                        };

                        await request(options, function(error, response, body) {
                            if (error) throw new Error(error);
                            
                            address="";
                            // console.log('body', body);
                            // console.log((parseFloat(body)/100000000).toFixed(8));
                            //body = JSON.parse(body);
                            let btcBlc = (parseFloat(body)/100000000).toFixed(8);
                            if(btcBlc!="NaN"){
                                //console.log("plance", btcBlc);
                                totalBal=parseFloat(totalBal)+parseFloat(btcBlc);
                                console.log('totalBal', totalBal);
                                //console.log('S.No', i);
                                if(index==(users.length-1)){
                                    res.status(200).send({balance: totalBal});
                                }
                            }
                        })
                    }
                }
            })
        }, allDone);
    })
})

//Send OTP
app.post('/sendOTP', urlencodedParser, function(req, res) {
    console.log(req.body.umail);
    Users.find({ 'umail': req.body.umail }, function(err, users) {
        console.log(users);
        if (err) return console.error(err);
        if (users[0]) {
            let pin = Math.floor(100000 + Math.random() * 900000);
            pin = String(pin);
            pin = pin.substring(0,4);
            Users.update({ 'umail': req.body.umail }, { $set: { 'pin': pin } }, function(err, user) {
                if (err) return console.error(err);
                var mailOptions = {
                    from: "info@cryptopocket.info",
                    to: req.body.umail,
                    subject: 'One Time Password',
                    html: 'Your OTP is ' + pin,
                };
                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message sent:', info)
                });
                data = {
                    'status': 1,
                    'msg': 'send otp'
                }
                res.status(200).send(data);
            })
        } else {
            data = {
                'status': 'err_userinfo'
            }
            res.status(200).send(data);
        }
    })
})

//verify user OTP
app.post('/verifyOTP', urlencodedParser, function(req, res) {
    console.log(req.body);
    if (req.body.uid == undefined && req.body.otp == undefined) {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    } else if (req.body.uid == "" || req.body.otp == "") {
        data = {
            'status': 'err_null_param'
        }
        res.status(200).send(data);
        return;
    } else {
        Users.find({ '_id': req.body.uid, 'pin': req.body.otp }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                data = {
                    'status': 1,
                    'msg': 'otp verified'
                }
                res.status(200).send(data);
            } else {
                data = {
                    'status': 'err_userinfo'
                }
                res.status(200).send(data);
            }
        })
    }
})

app.post('/usersById', urlencodedParser, function(req, res) {
        Users.find({ '_id': req.body.uid }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                UserAddresses.find({ 'u_id': users[0]._id }, function(err, info) {
                    if (err) {
                        data = {
                            'status': 'err_addrinfo'
                        }
                        res.status(200).send(data);
                    } else {
                        data = {
                            'status': '1',
                            'user_info': users[0],
                            'addr_info': info[0]
                        }
                        res.status(200).send(data);
                    }
                })
            } else {
                data = {
                    'status': 'err_userinfo'
                }
                res.status(200).send(data);
            }
        })
    })
//Forget Pin
app.post('/forgotPin', urlencodedParser, function(req, res) {
    console.log(req.body.umail);
    Users.find({ 'umail': req.body.umail }, function(err, users) {
        console.log(users);
        if (err) return console.error(err);
        if (users[0]) {
            if (users[0].pin != '' && users[0].pin != undefined) {
                var mailOptions = {
                    from: "info@cryptopocket.info",
                    to: req.body.umail,
                    subject: 'Forgot Passcode',
                    html: 'Your Pass Code is ' + users[0].pin,
                };
                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message sent:', info)
                });
                data = {
                    'status': 1,
                    'pin': users[0].pin,
                    'msg': 'send passcode'
                }
                res.status(200).send(data);
            } else {
                pin = Math.floor(Math.random() * 9999) + 1000;
                Users.update({ 'umail': req.body.umail }, { $set: { 'pin': pin } }, function(err, user) {
                    if (err) return console.error(err);
                    var mailOptions = {
                        from: "info@cryptopocket.info",
                        to: req.body.umail,
                        subject: 'Forgot Password',
                        html: 'Your Pass Code is ' + pin,
                    };
                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Message sent:', info)
                    });
                    data = {
                        'status': 1,
                        'pin': pin,
                        'msg': 'send passcode'
                    }
                    res.status(200).send(data);
                })
            }
        } else {
            data = {
                'status': 'err_userinfo'
            }
            res.status(200).send(data);
        }
    })
})
app.post('/usersAddr', urlencodedParser, function(req, res) {
    Users.find({ $or: [{ 'umail': req.body.umail }, { 'mobile_no': req.body.mobile_no }] }, function(err, users) {
        if (err) return console.error(err);
        if (users[0]) {
            UserAddresses.find({ 'u_id': users[0]._id }, function(err, info) {
                if (err) {
                    data = {
                        'status': 'err_addrinfo'
                    }
                    res.status(200).send(data);
                } else {
                    data = {
                        'status': '1',
                        'user_info': users[0],
                        'addr_info': info[0]
                    }
                    res.status(200).send(data);
                }
            })
        } else {
            data = {
                'status': 'err_userinfo'
            }
            res.status(200).send(data);
        }
    })
})

async function getBtcTxStatus(txid, addr){
    var options = {
        method: 'GET',
        url: 'https://bitaps.com/api/transaction/'+txid,
    };
    var type = '';
    await request(options, function (error, response, body) {
        if (error) throw new Error(error);

        body = JSON.parse(body);
        console.log(body);
        output = body.output;
        input = body.input;
        
        for (let index = 0; index < input.length; index++) {
            if(input[index].address==addr){
                type = 'out';
            }
        }
        for (let i = 0; i < output.length; i++) {
            if(output[index].address==addr){
                type = 'in';
            }
        }
    });
    return type;
}

app.get('/txs/:addr', urlencodedParser, async function(req, res) {
    console.log("call txs api for - "+req.params.addr);
    var options = {
        method: 'GET',
        url: 'https://blockchain.info/rawaddr/'+req.params.addr
    };
    let addr = req.params.addr;
    let txs=[];
    await request(options, async function(error, response, body) {
        body = JSON.parse(body);
        for (let index = 0; index < body.txs.length; index++) {
            let trans = body.txs[index];
            let theDate = new Date(trans.time * 1000);
            let dateString = theDate.toLocaleString();
            let btype="";
            let value=0;
            let output = trans.out;
            let input = trans.inputs;
            
            console.log("input", input);
            console.log("output", output);
            for (let j = 0; j < input.length; j++) {
                if(input[j].prev_out){
                    if(input[j].prev_out.spent == true && input[j].prev_out.addr != addr){
                        btype = 'in';
                        value = (input[j].prev_out.value/100000000).toFixed(8);
                        break;
                    }
                }else{
                    if(input[j].addr!=addr){
                        btype = 'in';
                        value = (input[j].value/100000000).toFixed(8);
                        break;
                    }
                }
            }
            if(btype==""){
                for (let i = 0; i < output.length; i++) {
                    if(output[i].prev_out){
                        if(output[i].prev_out.spent == false && output[i].prev_out.addr==addr){
                            btype = 'out';
                            value = (output[i].prev_out.value/100000000).toFixed(8);
                            break;
                        }
                    }else{
                        if(output[i].addr==addr){
                            btype = 'out';
                            value = (output[i].value/100000000).toFixed(8);
                            break;
                        }
                    }
                }
            }else{
                for (let i = 0; i < output.length; i++) {
                    if(output[i].prev_out){
                        if(output[i].prev_out.spent == false && output[i].prev_out.addr==addr){
                            value = (output[i].prev_out.value/100000000).toFixed(8);
                            break;
                        }
                    }else{
                        if(output[i].addr==addr){
                            btype = 'out';
                            value = (output[i].value/100000000).toFixed(8);
                            break;
                        }
                    }
                }
            }
            resp = {
                "amount": value.toString(),
                "txid": trans.hash,
                "coin_type": "BTC",
                "link": "https://www.blockchain.com/btc/tx/" + trans.hash,
                "datetime": dateString,
                "type": btype
            }
            txs.push(resp);
            if((index+1) == body.txs.length){
                res.send(txs);
            }else{
                console.log("loop time - "+index+ " and length is - "+body.txs.length);
            }
        }
    })
})

//GET ALL TRANSACTION PARTICULAR ADDRESS
app.post('/userAllTransaction', urlencodedParser, async function(req, res) {
    console.log("userAllTransaction");
    UserAddresses.find({ 'u_id': req.body.uid }, async function(err, info) {
        if (err) return console.error(err);
        if (info[0]) {
            let btcTrans = [];
            let amount=0;
            let tx_id = '';
            
            let addr = info[0].bitcoin_address;
            var options = {
                method: 'GET',
                url: 'https://blockchain.info/rawaddr/'+addr
            };
            let txs=[];
            await request(options, async function(error, response, body) {
                if (error) throw new Error(error);

                
                body = JSON.parse(body);
                let totalTran = body;
                //console.log("bitpay txs - ", body);
                let txs = totalTran.txs;
                if (txs.length == 0) {
                    //console.log(btcTrans);
                    var options = {
                        method: 'GET',
                        url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getTransByAddr',
                        qs: { addr: info[0].btc_address }
                    };

                    await request(options, async function(error, response, body) {
                        if (error) throw new Error(error);

                        let totalTran = JSON.parse(body);
                        console.log(totalTran);
                        let txs = totalTran;
                        if(txs.length==0){
                            data = {
                                'status': '1',
                                'transaction': btcTrans
                            }
                            res.status(200).send(data);
                        }else{
                            for (let index = 0; index < txs.length; index++) {
                                var theDate = new Date(txs[index].time * 1000);
                                dateString = theDate.toLocaleString();
                                balance = txs[index].amount;
                                if (txs[index].amount < 0) {
                                    txs[index].amount = Math.abs(balance);
                                }
                                resp = {
                                    "amount": txs[index].amount.toString(),
                                    "txid": txs[index].txid,
                                    "coin_type": "FORK",
                                    "link": "http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/tx/" + txs[index].txid,
                                    "datetime": dateString,
                                    "type": (txs[index].category=="send") ? 'out' : 'in'
                                }
                                
                                if(txs[index].amount.toString()=="0.025" && txs[index].category=="send" ){
                                    console.log("this is fee transaction - ", resp);
                                }else{
                                    btcTrans.push(resp);
                                }
                                amount = txs[index].amount;
                                tx_id = txs[index].txid;

                                txtLn2 = txs.length - 1;
                                if (index == txtLn2) {
                                    data = {
                                        'status': '1',
                                        'transaction': btcTrans
                                    }
                                    res.status(200).send(data);
                                } 
                            }
                        }
                    });
                } else {
                    for (let index = 0; index < body.txs.length; index++) {
                        let trans = body.txs[index];
                        let dateString = "";
                        let theDate = new Date(trans.time * 1000);
                        dateString = theDate.toLocaleString();
                        let btype="";
                        let value=0;
                        let output = trans.out;
                        let input = trans.inputs;
                        
                        for (let j = 0; j < input.length; j++) {
                            if(input[j].prev_out){
                                if(input[j].prev_out.spent == true && input[j].prev_out.addr != addr){
                                    btype = 'in';
                                    value = (input[j].prev_out.value/100000000).toFixed(8);
                                    break;
                                }
                            }else{
                                if(input[j].addr!=addr){
                                    btype = 'in';
                                    value = (input[j].value/100000000).toFixed(8);
                                    break;
                                }
                            }
                        }
                        if(btype==""){
                            for (let i = 0; i < output.length; i++) {
                                if(output[i].prev_out){
                                    if(output[i].prev_out.spent == false && output[i].prev_out.addr!=addr){
                                        btype = 'out';
                                        val = (output[i].prev_out.value/100000000).toFixed(8);
                                        value = parseFloat(value)+parseFloat(val);
                                    }
                                }else{
                                    if(output[i].addr!=addr){
                                        btype = 'out';
                                        val = (output[i].value/100000000).toFixed(8);
                                        value = parseFloat(value)+parseFloat(val);
                                    }
                                }
                                if(i==output.length-1)
                                value = (parseFloat(value)+parseFloat(0.0001)).toFixed(8);
                            }
                        }else{
                            for (let i = 0; i < output.length; i++) {
                                if(output[i].prev_out){
                                    if(output[i].prev_out.spent == false && output[i].prev_out.addr==addr){
                                        value = (output[i].prev_out.value/100000000).toFixed(8);
                                        break;
                                    }
                                }else{
                                    if(output[i].addr==addr){
                                        value = (output[i].value/100000000).toFixed(8);
                                        break;
                                    }
                                }
                            }
                        }
                        resp = {
                            "amount": value.toString(),
                            "txid": trans.hash,
                            "coin_type": "BTC",
                            "link": "https://www.blockchain.com/btc/tx/" + trans.hash,
                            "datetime": dateString,
                            "type": btype
                        }
                        btcTrans.push(resp);
                        if((index+1) == body.txs.length){
                            var options = {
                                method: 'GET',
                                url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getTransByAddr',
                                qs: { addr: info[0].btc_address }
                            };

                            await request(options, async function(error, response, body) {
                                if (error) throw new Error(error);

                                let totalTran = JSON.parse(body);
                                let txs = totalTran;
                                if(txs.length==0){
                                    console.log("0", btcTrans);
                                    data = {
                                        'status': '1',
                                        'transaction': btcTrans
                                    }
                                    res.status(200).send(data);
                                }else{
                                    for (let index = 0; index < txs.length; index++) {
                                        var theDate1 = new Date(txs[index].time * 1000);
                                        dateString1 = theDate1.toLocaleString();
                                        balance = txs[index].amount;
                                        if (txs[index].amount < 0) {
                                            txs[index].amount = Math.abs(balance);
                                        }
                                        resp = {
                                            "amount": txs[index].amount.toString(),
                                            "txid": txs[index].txid,
                                            "coin_type": "FORK",
                                            "link": "http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/tx/" + txs[index].txid,
                                            "datetime": dateString1,
                                            "type": (txs[index].category=="send") ? 'out' : 'in'
                                        }
                                        
                                        if(txs[index].amount.toString()=="0.025" && txs[index].category=="send" ){
                                            console.log("this is fee transaction - ", resp);
                                        }else{
                                            btcTrans.push(resp);
                                        }
                                        amount = txs[index].amount;
                                        tx_id = txs[index].txid;
        
                                        txtLn2 = txs.length - 1;
                                        if (index == txtLn2) {
                                            console.log(btcTrans);
                                            data = {
                                                'status': '1',
                                                'transaction': btcTrans
                                            }
                                            res.status(200).send(data);
                                        } 
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
    })
})


//GET ALL TRANSACTION OF BITCOIN
app.post('/btcTxHistory', urlencodedParser, async function(req, res) {
    console.log("btcTxHistory");
    UserAddresses.find({ 'u_id': req.body.uid }, async function(err, info) {
        if (err) return console.error(err);
        if (info[0]) {
            let btcTrans = [];
            let amount=0;
            let tx_id = '';
            
            let addr = info[0].bitcoin_address;
            var options = {
                method: 'GET',
                url: 'https://blockchain.info/rawaddr/'+addr
            };
            let txs=[];
            await request(options, async function(error, response, body) {
                if (error) throw new Error(error);

                
                body = JSON.parse(body);
                let totalTran = body;
                //console.log("bitpay txs - ", body);
                let txs = totalTran.txs;
                if (txs.length == 0) {
                    data = {
                        'status': '1',
                        'transaction': btcTrans
                    }
                    res.status(200).send(data);
                } else {
                    for (let index = 0; index < body.txs.length; index++) {
                        let trans = body.txs[index];
                        let dateString = "";
                        let theDate = new Date(trans.time * 1000);
                        dateString = theDate.toLocaleString();
                        let btype="";
                        let value=0;
                        let output = trans.out;
                        let input = trans.inputs;
                        
                        for (let j = 0; j < input.length; j++) {
                            if(input[j].prev_out){
                                if(input[j].prev_out.spent == true && input[j].prev_out.addr != addr){
                                    btype = 'in';
                                    value = (input[j].prev_out.value/100000000).toFixed(8);
                                    break;
                                }
                            }else{
                                if(input[j].addr!=addr){
                                    btype = 'in';
                                    value = (input[j].value/100000000).toFixed(8);
                                    break;
                                }
                            }
                        }
                        if(btype==""){
                            for (let i = 0; i < output.length; i++) {
                                if(output[i].prev_out){
                                    if(output[i].prev_out.spent == false && output[i].prev_out.addr!=addr){
                                        btype = 'out';
                                        val = (output[i].prev_out.value/100000000).toFixed(8);
                                        value = parseFloat(value)+parseFloat(val);
                                    }
                                }else{
                                    if(output[i].addr!=addr){
                                        btype = 'out';
                                        val = (output[i].value/100000000).toFixed(8);
                                        value = parseFloat(value)+parseFloat(val);
                                    }
                                }
                                if(i==output.length-1)
                                value = (parseFloat(value)+parseFloat(0.0001)).toFixed(8);
                            }
                        }else{
                            for (let i = 0; i < output.length; i++) {
                                if(output[i].prev_out){
                                    if(output[i].prev_out.spent == false && output[i].prev_out.addr==addr){
                                        value = (output[i].prev_out.value/100000000).toFixed(8);
                                        break;
                                    }
                                }else{
                                    if(output[i].addr==addr){
                                        value = (output[i].value/100000000).toFixed(8);
                                        break;
                                    }
                                }
                            }
                        }
                        resp = {
                            "amount": value.toString(),
                            "txid": trans.hash,
                            "coin_type": "BTC",
                            "link": "https://www.blockchain.com/btc/tx/" + trans.hash,
                            "datetime": dateString,
                            "type": btype
                        }
                        btcTrans.push(resp);
                        if((index+1) == body.txs.length){
                            data = {
                                'status': '1',
                                'transaction': btcTrans
                            }
                            res.status(200).send(data);
                        }
                    }
                }
            });
        }
    })
})


//GET ALL TRANSACTION OF FORKCOIN
app.post('/frkTxHistory', urlencodedParser, async function(req, res) {
    console.log("btcTxHistory");
    UserAddresses.find({ 'u_id': req.body.uid }, async function(err, info) {
        if (err) return console.error(err);
        if (info[0]) {
            let btcTrans = [];
            let amount=0;
            let tx_id = '';
            
            let addr = info[0].btc_address;
            var options = {
                method: 'GET',
                url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getTransByAddr',
                qs: { addr: info[0].btc_address }
            };

            await request(options, async function(error, response, body) {
                if (error) throw new Error(error);

                let totalTran = JSON.parse(body);
                let txs = totalTran;
                if(txs.length==0){
                    console.log("0", btcTrans);
                    data = {
                        'status': '1',
                        'transaction': btcTrans
                    }
                    res.status(200).send(data);
                }else{
                    for (let index = 0; index < txs.length; index++) {
                        var theDate1 = new Date(txs[index].time * 1000);
                        dateString1 = theDate1.toLocaleString();
                        balance = txs[index].amount;
                        if (txs[index].amount < 0) {
                            txs[index].amount = Math.abs(balance);
                        }
                        resp = {
                            "amount": txs[index].amount.toString(),
                            "txid": txs[index].txid,
                            "coin_type": "FORK",
                            "link": "http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/tx/" + txs[index].txid,
                            "datetime": dateString1,
                            "type": (txs[index].category=="send") ? 'out' : 'in'
                        }
                        
                        if(txs[index].amount.toString()=="0.025" && txs[index].category=="send" ){
                            console.log("this is fee transaction - ", resp);
                        }else{
                            btcTrans.push(resp);
                        }
                        amount = txs[index].amount;
                        tx_id = txs[index].txid;

                        txtLn2 = txs.length - 1;
                        if (index == txtLn2) {
                            console.log(btcTrans);
                            data = {
                                'status': '1',
                                'transaction': btcTrans
                            }
                            res.status(200).send(data);
                        } 
                    }
                }
            });
        }
    })
})

//GET TRANSACTION PARTICULAR USER

app.post('/userTransaction', urlencodedParser, function(req, res) {
    UserAddresses.find({ 'u_id': req.body.uid }, function(err, info) {
        if (err) return console.error(err);
        if (info[0]) {
            Transaction.find({ $or: [{ 'from_address': info[0].btc_address }, { 'to_address': info[0].btc_address }, { 'from_address': info[0].bitcoin_address }, { 'to_address': info[0].bitcoin_address }] }).sort({ datetime: -1 }).exec(function(err, transaction) {
                //if (err) return console.error(err);
                if (err) {
                    data = {
                        'status': 'err_gettransaction'
                    }
                    res.status(200).send(data);
                } else {
                    console.log(transaction);
                    data = {
                        'status': '1',
                        'transaction': transaction
                    }
                    res.status(200).send(data);
                }
            })
        }
    })
})

app.post('/userTransactionTest', urlencodedParser, function(req, res) {
    UserAddresses.find({ 'u_id': req.body.uid }, function(err, info) {
        if (err) return console.error(err);
        if (info[0]) {
            if (req.body.start == undefined || req.body.start == 0) {
                req.body.start = 0
            }
            if (req.body.end == undefined || req.body.end == 0) {
                req.body.end = 10
            }
            console.log(req.body.start + " and " + req.body.end);
            Transaction.find({ $or: [{ 'from_address': info[0].btc_address }, { 'to_address': info[0].btc_address }, { 'from_address': info[0].ltc_address }, { 'to_address': info[0].ltc_address }] }).skip(parseInt(req.body.start)).limit(parseInt(req.body.end)).sort({ datetime: -1 }).exec(function(err, transaction) {
                if (err) return console.error(err);
                if (err) {
                    data = {
                        'status': 'err_gettransaction'
                    }
                    res.status(200).send(data);
                } else {
                    var dt = dateTime.create();
                    var formatted = dt.format('Y-m-d H:M:S');
                    data = {
                        'status': '1',
                        'transaction': transaction
                    }
                    res.status(200).send(data);
                }
            })
        }
    })
})

//GET TRANSACTION PERTICULAR USER

app.post('/forgetPwd', urlencodedParser, function(req, res) {
    Users.find({ 'umail': req.body.umail }, function(err, users) {
        if (err) return console.error(err);
        if (users[0]) {
            var pwd = Math.floor(100000 + Math.random() * 900000).toString();
            const saltRounds = 10
            var upassword = bcrypt.hashSync(pwd, saltRounds);
            const sendmail = require('sendmail')();
            Users.update({ 'umail': req.body.umail }, { $set: { user_password: upassword } }, function(err, user) {
                if (err) return console.error(err);
                var mailOptions = {
                    from: "info@cryptopocket.info",
                    to: req.body.umail,
                    subject: 'Forgot Password',
                    html: 'Your Password is ' + pwd,
                };
                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }else{
                        console.log('Message sent:', info)
                        data = {
                            'status': 1,
                            'password': pwd,
                            'msg': 'password changed'
                        }
                        res.status(200).send(data);
                    }
                });
            })
        } else {
            data = {
                'status': 'err_userinfo'
            }
            res.status(200).send(data);
        }
    })
})

app.post('/changePwd', urlencodedParser, function(req, res) {
    if (req.body.uid == undefined && req.body.new_password == undefined) {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    }
    if (req.body.uid == "" || req.body.new_password == "") {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    }
    Users.find({ '_id': req.body.uid }, function(err, users) {
        if (err) return console.error(err);
        if (users[0]) {
            var pwd = req.body.new_password;
            const saltRounds = 10
            var upassword = bcrypt.hashSync(pwd, saltRounds);
            const sendmail = require('sendmail')();
            Users.update({ '_id': req.body.uid }, { $set: { user_password: upassword } }, function(err, user) {
                if (err) return console.error(err);

                data = {
                    'status': 1,
                    'password': pwd,
                    'msg': 'password changed'
                }
                res.status(200).send(data);
            })
        } else {
            data = {
                'status': 'err_userinfo'
            }
            res.status(200).send(data);
        }
    })
})

//update registration token
app.post('/updateRegistrationToken', urlencodedParser, function(req, res) {
    console.log(req.body);
    if (req.body.uid == undefined && req.body.token == undefined) {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    } else if (req.body.uid == "" || req.body.token == "") {
        data = {
            'status': 'err_null_param'
        }
        res.status(200).send(data);
        return;
    } else {
        Users.find({ '_id': req.body.uid }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                Users.update({ '_id': req.body.uid }, { $set: { token: req.body.token } }, function(err, user) {
                    if (err) return console.error(err);

                    data = {
                        'status': 1,
                        'token': req.body.token,
                        'msg': 'token changed'
                    }
                    res.status(200).send(data);
                })
            } else {
                data = {
                    'status': 'err_userinfo'
                }
                res.status(200).send(data);
            }
        })
    }
})


//update user pass code
app.post('/updatePassCode', urlencodedParser, function(req, res) {
    console.log(req.body);
    if (req.body.uid == undefined && req.body.pin == undefined) {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    } else if (req.body.uid == "" || req.body.pin == "") {
        data = {
            'status': 'err_null_param'
        }
        res.status(200).send(data);
        return;
    } else {
        Users.find({ '_id': req.body.uid }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                Users.update({ '_id': req.body.uid }, { $set: { pin: req.body.pin } }, function(err, user) {
                    if (err) return console.error(err);

                    data = {
                        'status': 1,
                        'pin': req.body.pin,
                        'msg': 'passcode updated'
                    }
                    res.status(200).send(data);
                })
            } else {
                data = {
                    'status': 'err_userinfo'
                }
                res.status(200).send(data);
            }
        })
    }
})

//verify user pass code
app.post('/verifyPassCode', urlencodedParser, function(req, res) {
    console.log(req.body);
    if (req.body.uid == undefined && req.body.pin == undefined) {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    } else if (req.body.uid == "" || req.body.pin == "") {
        data = {
            'status': 'err_null_param'
        }
        res.status(200).send(data);
        return;
    } else {
        Users.find({ '_id': req.body.uid, 'pin': req.body.pin }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                data = {
                    'status': 1,
                    'pin': req.body.pin,
                    'msg': 'passcode verified'
                }
                res.status(200).send(data);
            } else {
                data = {
                    'status': 'err_userinfo'
                }
                res.status(200).send(data);
            }
        })
    }
})


/*****Sign Up******/
app.post('/register', urlencodedParser, function(req, res) {
        var data
        console.log(req.body);
        //password hash
        const saltRounds = 10
        var upassword = bcrypt.hashSync(req.body.user_password, saltRounds);

        var userData = {
            username: req.body.username,
            umail: req.body.umail,
            mobile_no: req.body.mobile_no,
            user_type: req.body.user_type,
            user_password: upassword
        }

        //check for email address and mobile no
        Users.find({ $or: [{ 'umail': req.body.umail }, { 'mobile_no': req.body.mobile_no }] }, function(err, users) {
                if (err) return console.error(err);
                if (users.length > 0) {
                    if (users[0].mobile_no == req.body.mobile_no) {
                        data = {
                            'status': 'err_mobile'
                        }
                        res.status(200).send(data);
                    } else if (users[0].umail == req.body.umail) {
                        data = {
                            'status': 'err_email'
                        }
                        res.status(200).send(data);
                    }
                } else {
                    //save data to users collection
                    Users.create(userData, function(err, user) {
                            if (err) {
                                data = {
                                    'status': '0'
                                }
                                console.log(err);
                            } else {
                                //generate Bitcoin Address
                                //get rand number
                                /*var rand_buffer = Btcore.crypto.Random.getRandomBuffer(32);
                                var rand_num = Btcore.crypto.BN.fromBuffer(rand_buffer);

                                //get privateKey	
                                var priv_key = new Btcore.PrivateKey(rand_num,'testnet');

                                //get address
                                var Addr = priv_key.toAddress().toString();
					
                                priv_key = priv_key.toWIF();
					
                                var alength = Addr.length;
					
                                var cryptr = new Cryptr(Addr.slice(0,2)+Addr.slice((alength/2),((alength/2)+2))+Addr.slice((alength-2),alength));
                                priv_key = cryptr.encrypt(priv_key);

                                var privateKey = new litecore.PrivateKey("testnet");

                                var ltc_Addr = privateKey.toAddress().toString();
                                var ltc_priv_key = privateKey.toWIF();
                                var cryptr = new Cryptr(ltc_Addr.slice(0,2)+ltc_Addr.slice((alength/2),((alength/2)+2))+ltc_Addr.slice((alength-2),alength));
                                ltc_priv_key = cryptr.encrypt(ltc_priv_key); */

                                var options = {
                                    method: 'GET',
                                    url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getAddress'
                                };

                                request(options, function(error, response, body) {
                                    if (error) throw new Error(error);
                                    console.log(body);
                                    var result = JSON.parse(body);
                                    console.log(result);

                                    var Addr = result.address;
                                    var priv_key = result.privateKey;

                                    var alength = Addr.length;

                                    var cryptr = new Cryptr(Addr.slice(0, 2) + Addr.slice((alength / 2), ((alength / 2) + 2)) + Addr.slice((alength - 2), alength));
                                    priv_key = cryptr.encrypt(priv_key);

                                    var addinfo = {
                                            u_id: user._id,
                                            btc_address: Addr,
                                            btc_privkey: priv_key
                                                //ltc_address: ltc_Addr,
                                                //ltc_privkey: ltc_priv_key
                                        }
                                        //add data to UserAddresses collection
                                    UserAddresses.create(addinfo, function(err, info) {
                                        if (err) {
                                            data = {
                                                'status': '0'
                                            }
                                        } else {

                                            data = {
                                                'status': '1',
                                                'uid': user._id
                                            }
                                        }
                                        //console.log(data);
                                        res.status(200).send(data);
                                    })
                                }); //get address

                            } //end else
                        }) //save user end

                } //else colse

            }) // find close

    }) //register api close


//api for user login
app.post('/login', urlencodedParser, function(req, res) {
        console.log(req.body);
        var mobile_no, umail, data;
        if (isNaN(req.body.ulogin)) {
            console.log("umail");
            console.log(req.body);
            //find user if user user login by emauil address
            Users.find({ 'umail': req.body.ulogin }, function(err, users) {
                console.log("exist user table ");
                console.log(users);
                if (err) {
                    data = {
                            'status': 'err_finduser'
                        }
                        //send response
                    res.status(200).send(data);
                }
                console.log("exist user table ");
                //res.status(200).send(users);
                //return;
                if (users.length == 1) {
                    console.log("user find " + users[0]._id);
                    if (bcrypt.compareSync(req.body.upass, users[0].user_password)) {
                        UserAddresses.find({ 'u_id': users[0]._id }, function(err, info) {
                            if (err) {
                                data = {
                                    'status': 'err_addrinfo'
                                }
                                console.log(err);
                            } else {
                                data = {
                                    'status': '1',
                                    'user_info': users[0],
                                    'addr_info': info[0]
                                }
                            }
                            //send response
                            res.status(200).send(data);
                        });
                    } else {
                        data = {
                                'status': 'wrong_pass'
                            }
                            //send response
                        res.status(200).send(data);
                    }
                } else {

                    console.log("user not find ");
                    data = {
                            'status': 'not_reg'
                        }
                        //send response
                    res.status(200).send(data);
                } //else end	

            }); //user find end

        } else {
            console.log("number");
            //find user if user user login by mobile no
            Users.find({ 'mobile_no': req.body.ulogin }, function(err, users) {
                if (err) {
                    data = {
                            'status': 'err_finduser'
                        }
                        //send response
                    res.status(200).send(data);
                }
                //console.log(users[0]);
                //return;
                if (users.length == 1) {
                    if (bcrypt.compareSync(req.body.upass, users[0].user_password)) {
                        UserAddresses.find({ 'u_id': users[0]._id }, function(err, info) {
                            if (err) {
                                data = {
                                        'status': 'err_addrinfo'
                                    }
                                    //console.log(err);
                            } else {
                                data = {
                                    'status': '1',
                                    'user_info': users[0],
                                    'addr_info': info[0]
                                }
                            }
                            //send response
                            res.status(200).send(data);
                        });
                    } else {
                        data = {
                                'status': 'wrong_pass'
                            }
                            //send response
                        res.status(200).send(data);
                    } //else end
                } else {
                    data = {
                            'status': 'not_reg'
                        }
                        //send response
                    res.status(200).send(data);
                } //else end	

            }); // find user end
        } // else end

    }) // login end


//api to get all users
app.get('/getusers', urlencodedParser, function(req, res) {
    Users.find(function(err, users) {
        //if (err) return console.error(err);
        //console.log(users);
        res.status(200).send(users);
    });
})

/*****Get Bitcoin Balance******/
app.post('/getBalanceByExplorer', urlencodedParser, function(req, res) {
    //get values from request
    //console.log(req.body);
    var data;
    var uid = req.body.uid;
    var request = require("request");

    UserAddresses.find({ 'u_id': uid }, function(err, info) {
        if (err) {
            data = {
                'status': 'err'
            }
            res.status(200).send(data);
        } else {
            console.log(info[0]);
            // return;
            if (info.length > 0) {
                let frkBlc = 0;
                let btcBlc = 0;
                let btcRes = null;
                let frkRes = null;
                if (info[0].bitcoin_address != null) {
                    var options = {
                        method: 'GET',
                        url: 'https://insight.bitpay.com/api/addr/' + info[0].bitcoin_address + '/',
                        qs: { noTxList: '1' }
                    };

                    request(options, function(error, response, body) {
                        if (error) throw new Error(error);

                        console.log(body);
                        body = JSON.parse(body);
                        btcBlc = body.balance;
                        btcRes = {
                            name: 'BTC',
                            balance: btcBlc.toString(),
                            fee: '0.00015',
                            imageurl: 'https://en.bitcoin.it/w/images/en/2/29/BC_Logo_.png',
                            pvtKey: info[0].bitcoin_privkey,
                            address: info[0].bitcoin_address,
                            tx_api: 'btcTxProposal'
                        }


                        //url = 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getBalance?addr=' + info[0].btc_address;
                        url = 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/ext/getbalance/' + info[0].btc_address;
                        var options = {
                            method: 'GET',
                            url: url
                        };

                        request(options, function(error, response, body) {
                            //if (error) throw new Error(error);
                            var data;
                            console.log(body);
                            //body = JSON.parse(body);
                            if (isNaN(body)) {
                                frkBlc = '0.0';
                            } else {
                                frkBlc = body;
                            }
                            frkRes = {
                                name: 'FRK',
                                fee: '0.025',
                                balance: frkBlc.toString(),
                                imageurl: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/images/logo.png',
                                pvtKey: info[0].btc_privkey,
                                address: info[0].btc_address,
                                tx_api: 'bitcoinTxProposal'
                            }
                            data = [
                                btcRes,
                                frkRes
                            ]
                            response = {
                                status: 1,
                                data
                            }
                            res.status(200).send(response);
                        });
                    });
                } else {
                    //generate Bitcoin Address
                    //get rand number
                    var rand_buffer = Btcore.crypto.Random.getRandomBuffer(32);
                    var rand_num = Btcore.crypto.BN.fromBuffer(rand_buffer);

                    //get privateKey	
                    var priv_key = new Btcore.PrivateKey(rand_num, bitNetworkType);

                    //get address
                    var Addr = priv_key.toAddress().toString();

                    priv_key = priv_key.toWIF();

                    var alength = Addr.length;

                    var cryptr = new Cryptr(Addr.slice(0, 2) + Addr.slice((alength / 2), ((alength / 2) + 2)) + Addr.slice((alength - 2), alength));
                    priv_key = cryptr.encrypt(priv_key);

                    var addinfo = {
                        u_id: uid,
                        bitcoin_address: Addr,
                        bitcoin_privkey: priv_key
                    }
                    btcRes = {
                            name: 'BTC',
                            balance: '0',
                            fee: '0.00015',
                            imageurl: 'https://en.bitcoin.it/w/images/en/2/29/BC_Logo_.png',
                            pvtKey: priv_key,
                            address: Addr,
                            tx_api: 'btcTxProposal'
                        }
                        //add data to UserAddresses collection
                    UserAddresses.update({ 'u_id': uid }, { $set: addinfo }, function(err, users) {

                        url = 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/ext/getbalance/' + info[0].btc_address;
                        var options = {
                            method: 'GET',
                            url: url
                        };

                        request(options, function(error, response, body) {
                            //if (error) throw new Error(error);
                            var data;
                            console.log(body);
                            //body = JSON.parse(body);
                            if (isNaN(body)) {
                                frkBlc = '0.0';
                            } else {
                                frkBlc = body;
                            }
                            frkRes = {
                                name: 'FRK',
                                balance: frkBlc.toString(),
                                fee: '0.025',
                                imageurl: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/images/logo.png',
                                pvtKey: priv_key,
                                address: Addr,
                                tx_api: 'bitcoinTxProposal'
                            }

                            data = [
                                btcRes,
                                frkRes
                            ]
                            response = {
                                status: 1,
                                data
                            }
                            res.status(200).send(response);
                        });
                    })
                }
            } else {
                data = {
                    'status': 'not_found'
                }
                res.status(200).send(data);
            }
        } // end finnd UserAddresses else
    }); //end fine UserAddresses

});

/*****Get Bitcoin Balance******/
app.post('/getBalance', urlencodedParser, function(req, res) {
    var data;
    var uid = req.body.uid;
    var request = require("request");
    console.log("getBalance", req.body);
    let totalBalanceUSD=0;
    UserAddresses.find({ 'u_id': uid }, function(err, info) {
        if (err) {
            data = {
                'status': 'err'
            }
            res.status(200).send(data);
        } else {
            var options = { 
                method: 'GET',
                url: 'https://api.coingecko.com/api/v3/coins/bitcoin/tickers'
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);

                console.log(body);
                body = JSON.parse(body);
                let btcToUSD = body.tickers[0].converted_last.usd;
                var options = {
                    method: 'GET',
                    url: 'https://graphs2.coinmarketcap.com/currencies/forkcoin/'
                };

                request(options, function (error, response, body) {
                    if (error) throw new Error(error);

                    body = JSON.parse(body);
                    let frkToUSD = body.price_usd[body.price_usd.length-1][1];
                    console.log(body.price_usd[body.price_usd.length-1]);
                    // return;
                    if (info.length > 0) {
                        let frkBlc = 0;
                        let btcBlc = 0;
                        let btcRes = null;
                        let frkRes = null;
                        let uid = info[0].u_id;
                        if (info[0].bitcoin_address != null) {
                            var options = {
                                method: 'GET',
                                url: 'https://insight.bitpay.com/api/addr/' + info[0].bitcoin_address + '/',
                                qs: { noTxList: '1' }
                            };

                            request(options, function(error, response, body) {
                                if (error) throw new Error(error);
                                
                                console.log("up", body);
                                body = JSON.parse(body);
                                btcBlc = body.balance;
                                btcRes = {
                                    name: 'BTC',
                                    balance: btcBlc.toString(),
                                    fee: totalFeeBtc,
                                    imageurl: 'https://en.bitcoin.it/w/images/en/2/29/BC_Logo_.png',
                                    pvtKey: info[0].bitcoin_privkey,
                                    address: info[0].bitcoin_address,
                                    tx_api: 'btcTxProposal',
                                    txhistory_api: 'btcTxHistory'
                                }


                                url = 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getBalanceByUnspent?addr=' + info[0].btc_address;
                                var options = {
                                    method: 'GET',
                                    url: url
                                };

                                request(options, async function(error, response, body) {
                                    if (error) throw new Error(error);
                                    var data;
                                    console.log(body);
                                    if(body!="undefined" && body!=undefined){
                                        body = JSON.parse(body);
                                        frkBlc = body.balance;
                                    }else{
                                        frkBlc = 0;
                                    }
                                    frkRes = {
                                        name: 'FORK',
                                        fee: '0.025',
                                        balance: frkBlc.toString()=="NaN" ? "0.0" : frkBlc.toString(),
                                        imageurl: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/images/logo.png',
                                        pvtKey: info[0].btc_privkey,
                                        address: info[0].btc_address,
                                        tx_api: 'bitcoinTxProposal',
                                        txhistory_api: 'frkTxHistory'
                                    }
                                    if (info[0].btcp_address != null) {
                                        var options = {
                                            method: 'GET',
                                            url: 'https://explorer.btcprivate.org/api/addr/'+info[0].btcp_address+'/?noTxList=1',
                                        };
                                
                                        request(options, function(error, response, body) {
                                            if (error) throw new Error(error);
                                
                                            body = JSON.parse(body);
                                            btcBlc = body.balance;
                                            btgRes = {
                                                name: 'BTG',
                                                balance: "Coming Soon",
                                                fee: '0.5',
                                                imageurl: 'https://s2.coinmarketcap.com/static/img/coins/64x64/2083.png',
                                                pvtKey: "priv_key",
                                                address: "info[0].btg_address",
                                                tx_api: 'btgTxProposal',
                                                txhistory_api: 'btgTxHistory'
                                            }
                                            data = [
                                                btcRes,
                                                frkRes,
                                                btgRes
                                            ]

                                            totalBalanceUSD = (parseFloat(btcBlc)*parseFloat(btcToUSD) + parseFloat(frkBlc)*parseFloat(frkToUSD)).toFixed(2);
                                            response = {
                                                status: 1,
                                                totalBalanceUSD,
                                                data
                                            }
                                            console.log(response);
                                            res.status(200).send(response);
                                        });
                                    }else{
                                        btgRes = {
                                            name: 'BTG',
                                            balance: "Coming Soon",
                                            fee: '0.5',
                                            imageurl: 'https://s2.coinmarketcap.com/static/img/coins/64x64/2083.png',
                                            pvtKey: "priv_key",
                                            address: "info[0].btg_address",
                                            tx_api: 'btgTxProposal',
                                            txhistory_api: 'btgTxHistory'
                                        }
                                        data = [
                                            btcRes,
                                            frkRes,
                                            btgRes
                                        ]

                                        totalBalanceUSD = (parseFloat(btcBlc)*parseFloat(btcToUSD) + parseFloat(frkBlc)*parseFloat(frkToUSD)).toFixed(2);
                                        response = {
                                            status: 1,
                                            totalBalanceUSD,
                                            data
                                        }
                                        console.log(response);
                                        res.status(200).send(response);
                                    }
                                });
                            });
                        } else {
                            //generate Bitcoin Address
                            //get rand number
                            var rand_buffer = Btcore.crypto.Random.getRandomBuffer(32);
                            var rand_num = Btcore.crypto.BN.fromBuffer(rand_buffer);

                            //get privateKey	
                            var priv_key = new Btcore.PrivateKey(rand_num, bitNetworkType);

                            //get address
                            var Addr = priv_key.toAddress().toString();

                            priv_key = priv_key.toWIF();

                            var alength = Addr.length;

                            var cryptr = new Cryptr(Addr.slice(0, 2) + Addr.slice((alength / 2), ((alength / 2) + 2)) + Addr.slice((alength - 2), alength));
                            priv_key = cryptr.encrypt(priv_key);

                            var addinfo = {
                                u_id: uid,
                                bitcoin_address: Addr,
                                bitcoin_privkey: priv_key
                            }
                            btcRes = {
                                    name: 'BTC',
                                    balance: '0',
                                    fee: totalFeeBtc,
                                    imageurl: 'https://en.bitcoin.it/w/images/en/2/29/BC_Logo_.png',
                                    pvtKey: priv_key,
                                    address: Addr,
                                    tx_api: 'btcTxProposal',
                                    txhistory_api: 'btcTxHistory'
                                }
                                //add data to UserAddresses collection
                            UserAddresses.update({ 'u_id': uid }, { $set: addinfo }, function(err, users) {

                                url = 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getBalanceByUnspent?addr=' + info[0].btc_address;
                                var options = {
                                    method: 'GET',
                                    url: url
                                };

                                request(options, async function(error, response, body) {
                                    if (error) throw new Error(error);
                                    console.log(body);
                                    var data;
                                    body = JSON.parse(body);
                                    frkBlc = body.balance;
                                    frkRes = {
                                        name: 'FORK',
                                        balance: frkBlc.toString()=="NaN" ? "0.0" : frkBlc.toString(),
                                        fee: '0.025',
                                        imageurl: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com/images/logo.png',
                                        pvtKey: priv_key,
                                        address: Addr,
                                        tx_api: 'bitcoinTxProposal',
                                        txhistory_api: 'frkTxHistory'
                                    }

                                    if (info[0].btcp_address != null) {
                                        var options = {
                                            method: 'GET',
                                            url: 'https://explorer.btcprivate.org/api/addr/'+info[0].btcp_address+'/?noTxList=1',
                                        };
                                
                                        request(options, function(error, response, body) {
                                            if (error) throw new Error(error);
                                
                                            console.log(body);
                                            body = JSON.parse(body);
                                            btcBlc = body.balance;
                                            btgRes = {
                                                name: 'BTG',
                                                balance: "Coming Soon",
                                                fee: '0.5',
                                                imageurl: 'https://s2.coinmarketcap.com/static/img/coins/64x64/2083.png',
                                                pvtKey: "priv_key",
                                                address: "info[0].btg_address",
                                                tx_api: 'btgTxProposal',
                                                txhistory_api: 'btgTxHistory'
                                            }
                                            data = [
                                                btcRes,
                                                frkRes,
                                                btgRes
                                            ]

                                            totalBalanceUSD = (parseFloat(btcBlc)*parseFloat(btcToUSD) + parseFloat(frkBlc)*parseFloat(frkToUSD)).toFixed(2);
                                            response = {
                                                status: 1,
                                                totalBalanceUSD,
                                                data
                                            }
                                            console.log(response);
                                            res.status(200).send(response);
                                        });
                                    }else{
                                        btgRes = {
                                            name: 'BTG',
                                            balance: "Coming Soon",
                                            fee: '0.5',
                                            imageurl: 'https://s2.coinmarketcap.com/static/img/coins/64x64/2083.png',
                                            pvtKey: "priv_key",
                                            address: "info[0].btg_address",
                                            tx_api: 'btgTxProposal',
                                            txhistory_api: 'btgTxHistory'
                                        }
                                        data = [
                                            btcRes,
                                            frkRes,
                                            btgRes
                                        ]

                                        totalBalanceUSD = (parseFloat(btcBlc)*parseFloat(btcToUSD) + parseFloat(frkBlc)*parseFloat(frkToUSD)).toFixed(2);
                                        response = {
                                            status: 1,
                                            totalBalanceUSD,
                                            data
                                        }
                                        console.log(response);
                                        res.status(200).send(response);
                                    }
                                });
                            })
                        }
                    } else {
                        data = {
                            'status': 'not_found'
                        }
                        res.status(200).send(data);
                    }
                });
            });
        } // end finnd UserAddresses else
    }); //end fine UserAddresses

});
/*****get current btc price in INR******/
var get_current_bitcoin_price = function(callback) {
    var options = {
        method: 'GET',
        url: 'https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=INR'
    }
    var request = require('request');
    request(options, function(error, response, body) {
        if (error) throw new Error(error);
        callback(JSON.parse(body)[0].price_inr);
    });
}

/*****get current ltc price in INR******/
var get_current_coin_price = function(callback) {
    var options = {
        method: 'GET',
        url: 'https://api.coinbase.com/v2/prices/INR/spot',
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    };

    request(options, function(error, response, body) {
        //if (error) throw new Error(error);
        body = JSON.parse(body);
        console.log(body);
        data = {
            btc: body.data[0].amount,
            ltc: body.data[3].amount
        }
        callback(data);
    });
}

/*****get current ltc price in INR******/
var get_balance = function(res, addr) {

    url = 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getBalance?addr=' + addr;
    var options = {
        method: 'GET',
        url: url
    };

    request(options, function(error, response, body) {
        //if (error) throw new Error(error);
        var data;
        console.log(response);
        body = JSON.parse(body);
        var data = {
            'status': '1',
            'btc': {
                'btc': body.balance,
                //'balance': body,
                //'unconfirmedBalance': 0,
                //'btc': body,
                //'unconfirmed': 0
            }
        }

        res.status(200).send(data);
    });
    /*url = 'https://test-insight.bitpay.com/api/addr/'+addr+'?noTxList=1';
    var options = {
    	method: 'GET',
    	url: url
    };

    request(options, function(error, response, body) {
    	if (error) throw new Error(error);
    	var result1 = JSON.parse(body);
    	url = 'http://explorer.litecointools.com/api/addr/'+addr2+'?noTxList=1';
	
    	var options = {
    		method: 'GET',
    		url: url
    	};

    	request(options, function(error, response, body) {
    		if (error) throw new Error(error);
    		var result = JSON.parse(body);
    		get_current_coin_price( function(price){
    			var data = {
    				'status': '1',
    				'btc':{
    					'balance': Math.round(result1.balance * price.btc),
    					'unconfirmedBalance': Math.round(result1.unconfirmedBalance * price.btc),
    					'btc': result1.balance,
    					'unconfirmed': result1.unconfirmedBalance
    				},
    				'ltc':{
    					'balance': Math.round(result.balance * price.ltc),
    					'unconfirmedBalance': Math.round(result.unconfirmedBalance * price.ltc),
    					'btc': result.balance,
    					'unconfirmed': result.unconfirmedBalance
    				}
    			}
    			res.status(200).send(data);
    		}); 
    	});
    });*/

}

/*****Transactions For Forkcoin******/
app.post('/bitcoinTxProposal', urlencodedParser, function(req, res) {
    //get values from request
    console.log("transaction hit");
    var timestamp = req.body.timestamp;
    console.log("timestamp - " + timestamp);
    var data;
    let userFrom;
    let amountToBtc = 0;
    let toAddr = "";
    let toUserToken = "";
    let username = "";
    let to_user = "";
    let from_user = "";
    let privKeyFromFront = req.body.priv_key;
    if (req.body.uid == undefined && req.body.otp == undefined) {
        data = {
            'status': 'err_param'
        }
        res.status(200).send(data);
        return;
    } else if (req.body.uid == "" || req.body.otp == "") {
        data = {
            'status': 'err_null_param'
        }
        res.status(200).send(data);
        return;
    } else {
        Users.find({ '_id': req.body.uid, 'pin': req.body.otp }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                Users.find({ '_id': req.body.userid }, function(err, users) {
                    //if (err) return console.error(err);
                    //console.log(users);
                    if (err) return console.error(err);
                    if (users[0]) {
                        if (timestamp == users[0].timestamp) {
                            data = {
                                'status': 'err_timestamp'
                            }
                            res.status(200).send(data);
                        } else {
                            if (req.body.amount == undefined || req.body.amount == 0) {
                                data = {
                                    'status': 'amount_zero',
                                    'error': 'Amount must be greater then zero'
                                }
                                res.status(200).send(data);
                                return;
                            }

                            if (req.body.userid) {
                                var options = {
                                    method: 'POST',
                                    url: 'http://34.219.80.14:3131/usersById',
                                    headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                    form: { uid: req.body.userid }
                                };
                                request(options, function(error, response, body) {
                                    //if (error) throw new Error(error);
                                    body = JSON.parse(body);
                                    var priv_key = body.addr_info.btc_privkey;
                                    var fromAddr = body.addr_info.btc_address;

                                    username = body.user_info.username;
                                    userFrom = {
                                        "priv_key": priv_key,
                                        "fromAddr": fromAddr
                                    }

                                    var amount = req.body.amount;

                                    if (req.body.toAddr != "") {
                                        var options = {
                                            method: 'POST',
                                            url: 'http://34.219.80.14:3131/usersById',
                                            headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                            form: { uid: req.body.toAddr }
                                        };

                                        request(options, function(error, response, body) {
                                            //if (error) throw new Error(error);
                                            body = JSON.parse(body);
                                            toAddr = body.addr_info.btc_address;
                                            toUserToken = body.user_info.token;

                                        })
                                    }

                                    var fromAddr = userFrom.fromAddr;
                                    var priv_key = userFrom.priv_key;

                                    var options = {
                                        method: 'GET',
                                        url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/getBalance?addr=' + fromAddr
                                    };

                                    request(options, function(error, response, body) {
                                        //if (error) throw new Error(error);
                                        body = JSON.parse(body);

                                        var acAmount = parseFloat(body.balance);
                                        amount = parseFloat(amount);
                                        let fee = 0.025;
                                        //console.log(acAmount);
                                        //console.log(amount);
                                        let tAmt = parseFloat(amount + fee);
                                        if (acAmount < tAmt) {
                                            data = {
                                                'status': '0',
                                                'error': "insufficient balance"
                                            }
                                            res.status(200).send(data);
                                        } else {
                                            //console.log("sufficient funds");

                                            var alength = fromAddr.length;
                                            var cryptr = new Cryptr(fromAddr.slice(0, 2) + fromAddr.slice((alength / 2), ((alength / 2) + 2)) + fromAddr.slice((alength - 2), alength));
                                            privKeyFromFront = cryptr.decrypt(privKeyFromFront);
                                            console.log("privKeyFromFront", privKeyFromFront);

                                            var options = {
                                                method: 'GET',
                                                url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/sendTransaction?addrFrom=' + fromAddr + '&addrTo=' + req.body.toAddr + '&priv_key=' + privKeyFromFront + '&amount=' + amount
                                            };

                                            request(options, function(error, response, body) {
                                                //if (error) throw new Error(error);
                                                console.log(fromAddr);
                                                body = JSON.parse(body);
                                                console.log(body);
                                                if (body.error) {
                                                    data = {
                                                        'status': '0',
                                                        'error': "Error in Transaction"
                                                    }
                                                    res.status(200).send(data);
                                                } else {
                                                    var txid = body.txid;
                                                    let mainAddr = 'FDvy7uma82WSHkUexftoYENEEJdenpx8Lh';

                                                    var dt = dateTime.create();
                                                    var formatted = dt.format('Y-m-d H:M:S');
                                                    if (req.body.datetime == undefined) {
                                                        req.body.datetime = formatted;
                                                    }

                                                    var addinfo = {
                                                            u_id: req.body.userid,
                                                            from_address: userFrom.fromAddr,
                                                            to_address: req.body.toAddr,
                                                            amount: req.body.amount,
                                                            txid: txid,
                                                            status: 1,
                                                            coin_type: "FRK",
                                                            datetime: req.body.datetime
                                                        }
                                                        //add data to UserAddresses collection
                                                    Transaction.create(addinfo, function(err, info) {
                                                            var options = {
                                                                method: 'GET',
                                                                url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/sendTransaction?addrFrom=' + fromAddr + '&addrTo=' + mainAddr + '&priv_key=' + privKeyFromFront + '&amount=0.025'
                                                            };
                                                            request(options, function(error, response, body) {
                                                                console.log(fromAddr);
                                                                body = JSON.parse(body);
                                                                console.log(body);
                                                                if (err) {
                                                                    data = {
                                                                        'status': '0'
                                                                    }
                                                                } else {
                                                                    data = {
                                                                        'status': '1'
                                                                    }
                                                                }
                                                                res.status(200).send(data);
                                                            })
                                                        })
                                                        //res.status(200).send(data);
                                                }

                                            }); //
                                        }

                                    }); //check balance
                                });
                            }
                        }
                    } else {
                        data = {
                            'status': 'err_userinfo'
                        }
                        res.status(200).send(data);
                    }
                });
            } else {
                data = {
                    'status': 'err_userinfo'
                }
                res.status(200).send(data);
            }
        });
    }
});

/*****Transactions For Bitcoin******/
app.post('/btcTxProposal', urlencodedParser, function(req, res) {
    //get values from request
    console.log("transaction hit");

    var data;
    let userFrom;
    let amountToBtc = 0;
    let toAddr = req.body.toAddr;
    let toUserToken = "";
    let username = "";
    let to_user = "";
    let from_user = "";
    let privKeyFromFront = req.body.priv_key;

    if (req.body.uid == undefined && req.body.otp == undefined) {
        data = {
            'status': 'err_param',
            'error': 'required uid and otp'
        }
        res.status(200).send(data);
        return;
    } else if (req.body.uid == "" || req.body.otp == "") {
        data = {
            'status': 'err_null_param',
            'error': 'uid and otp should not be blank.'
        }
        res.status(200).send(data);
        return;
    } else {
        Users.find({ '_id': req.body.uid, 'pin': req.body.otp }, function(err, users) {
            if (err) return console.error(err);
            if (users[0]) {
                if (req.body.amount == undefined || req.body.amount == 0) {
                    data = {
                        'status': 'amount_zero',
                        'error': 'Amount must be greater then zero'
                    }
                    res.status(200).send(data);
                    return;
                }

                if (req.body.userid) {
                    var options = {
                        method: 'POST',
                        url: 'http://34.219.80.14:3131/usersById',
                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                        form: { uid: req.body.userid }
                    };
                    request(options, function(error, response, body) {
                        //if (error) throw new Error(error);
                        body = JSON.parse(body);
                        console.log(body);
                        var priv_key = body.addr_info.bitcoin_privkey;
                        var fromAddr = body.addr_info.bitcoin_address;

                        /*if(body.user_info.umail !="")
                            from_user = body.user_info.umail;
                        else if(body.user_info.mobile_no !="")
                            from_user = body.user_info.mobile_no;
                        else
                            from_user = body.user_info._id; */

                        username = body.user_info.username;
                        userFrom = {
                            "priv_key": priv_key,
                            "fromAddr": fromAddr
                        }

                        var amount = req.body.amount;

                        var fromAddr = userFrom.fromAddr;
                        var priv_key = userFrom.priv_key;

                        var options = {
                            method: 'GET',
                            url: bitcoinNodeUrl + '/api/addr/' + fromAddr + '?noTxList=1'
                        };

                        request(options, function(error, response, body) {
                            //if (error) throw new Error(error);
                            console.log(body);
                            body = JSON.parse(body);

                            var acAmount = parseFloat(body.balanceSat);
                            amount = parseFloat(amount);
                            var amountToBtc = Math.round(amount * 100000000);
                            //console.log(acAmount);
                            //console.log(amount);

                            if (acAmount < amountToBtc || acAmount <= totalFee) {
                                data = {
                                    'status': '0',
                                    'error': "insufficient balance"
                                }
                                res.status(200).send(data);
                            } else {
                                //console.log("sufficient funds");

                                var alength = fromAddr.length;
                                var cryptr = new Cryptr(fromAddr.slice(0, 2) + fromAddr.slice((alength / 2), ((alength / 2) + 2)) + fromAddr.slice((alength - 2), alength));
                                privKeyFromFront = cryptr.decrypt(privKeyFromFront);
                                console.log(privKeyFromFront);

                                //get private key for bitcoin
                                var priv_key = Btcore.PrivateKey.fromWIF(privKeyFromFront, bitNetworkType);

                                var insight = new InsightBitcoin(bitNetworkType);

                                if (acAmount >= amountToBtc + totalFee) {
                                    amountToBtc = amountToBtc;
                                } else {
                                    amountToBtc = amountToBtc - totalFee;
                                }
                                insight.getUnspentUtxos(fromAddr, function(error, utxos) {
                                    if (error) {
                                        data = {
                                            'status': '0',
                                            'error': error
                                        }
                                        res.status(200).send(data);
                                    } else {
                                        console.log(utxos);
                                        var tx = new Btcore.Transaction();
                                        tx.from(utxos);
                                        tx.to(toAddr, amountToBtc);
                                        tx.to("3P632HwJ1DQMvXzNZhC3DtmAuwKAefXjhf", 15000);
                                        tx.change(fromAddr);
                                        tx.fee(minerFee);
                                        tx.sign(priv_key);

                                        var txSerialized = tx.serialize();
                                        var data;

                                        insight.broadcast(txSerialized, function(err, txid) {
                                            if (err) {
                                                data = {
                                                    'status': '0',
                                                    'error': err
                                                }

                                            } else {
                                                /*data = {
                                                    'status': '1',
                                                    'txid': txid
                                                } */

                                                var dt = dateTime.create();
                                                var formatted = dt.format('Y-m-d H:M:S');
                                                if (req.body.datetime == undefined) {
                                                    req.body.datetime = formatted;
                                                }
                                                var addinfo = {
                                                        u_id: req.body.userid,
                                                        from_address: userFrom.fromAddr,
                                                        to_address: req.body.toAddr,
                                                        amount: req.body.amount,
                                                        txid: txid,
                                                        status: 1,
                                                        coin_type: "BTC",
                                                        datetime: req.body.datetime
                                                    }
                                                    //add data to UserAddresses collection
                                                Transaction.create(addinfo, function(err, info) {
                                                    if (err) {
                                                        data = {
                                                            'status': '0'
                                                        }
                                                    } else {

                                                        data = {
                                                            'status': '1',
                                                            'txid': txid
                                                        }
                                                    }
                                                    res.status(200).send(data);
                                                });
                                            }
                                            //res.status(200).send(data);
                                        }); //broadcast end
                                    }
                                });
                            } // check balance else

                        }); //check balance
                    });
                }
            } else {
                data = {
                    'status': 'err_otp',
                    'error': 'otp not valid'
                }
                res.status(200).send(data);
            }
        })
    }
});
/*****Transactions For Bitcoin Private******/
app.post('/btcpTxProposal', urlencodedParser, function(req, res) {
    //get values from request
    console.log("transaction hit");
    var timestamp = req.body.timestamp;
    console.log("timestamp - " + timestamp);
    var data;
    let userFrom;
    let amountToBtc = 0;
    let toAddr = "";
    let toUserToken = "";
    let username = "";
    let to_user = "";
    let from_user = "";
    let privKeyFromFront = req.body.priv_key;
    Users.find({ '_id': req.body.userid }, async function(err, users) {
        //if (err) return console.error(err);
        console.log(users);
        await Users.update({ '_id': req.body.userid }, { $set: {timestamp: timestamp} });
        if (err) return console.error(err);
        if (users[0]) {
            if (timestamp == users[0].timestamp) {
                data = {
                    'status': 'err_timestamp'
                }
                res.status(200).send(data);
            } else {
                if (req.body.amount == undefined || req.body.amount == 0) {
                    data = {
                        'status': 'amount_zero',
                        'error': 'Amount must be greater then zero'
                    }
                    res.status(200).send(data);
                    return;
                }

                if (req.body.userid) {
                    var options = {
                        method: 'POST',
                        url: 'http://34.219.80.14:3131/usersById',
                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                        form: { uid: req.body.userid }
                    };
                    request(options, function(error, response, body) {
                        //if (error) throw new Error(error);
                        body = JSON.parse(body);
                        var priv_key = body.addr_info.btcp_privkey;
                        var fromAddr = body.addr_info.btcp_address;

                        username = body.user_info.username;
                        userFrom = {
                            "priv_key": priv_key,
                            "fromAddr": fromAddr
                        }

                        var amount = req.body.amount;

                        if (req.body.toAddr != "") {
                            var options = {
                                method: 'POST',
                                url: 'http://34.219.80.14:3131/usersById',
                                headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                form: { uid: req.body.userid }
                            };

                            request(options, function(error, response, body) {
                                //if (error) throw new Error(error);
                                body = JSON.parse(body);
                                toAddr = body.addr_info.btcp_address;
                                toUserToken = body.user_info.token;

                            })
                        }

                        var fromAddr = userFrom.fromAddr;
                        var priv_key = userFrom.priv_key;

                        var options = {
                            method: 'GET',
                            url: 'https://explorer.btcprivate.org/api/addr/'+fromAddr+'/?noTxList=1'
                        };

                        request(options, function(error, response, body) {
                            //if (error) throw new Error(error);
                            body = JSON.parse(body);
                            //body.balance = body.received - body.send;
                            var acAmount = parseFloat(body.balance);
                            amount = parseFloat(amount);
                            let fee = 0.5;
                            //console.log(acAmount);
                            //console.log(amount);
                            let tAmt = parseFloat(amount + fee);
                            if (acAmount < tAmt) {
                                data = {
                                    'status': '0',
                                    'error': "insufficient balance"
                                }
                                res.status(200).send(data);
                            } else {
                                //console.log("sufficient funds");

                                var alength = fromAddr.length;
                                var cryptr = new Cryptr(fromAddr.slice(0, 2) + fromAddr.slice((alength / 2), ((alength / 2) + 2)) + fromAddr.slice((alength - 2), alength));
                                privKeyFromFront = cryptr.decrypt(privKeyFromFront);
                                console.log(privKeyFromFront);

                                var options = {
                                    method: 'GET',
                                    url: 'http://138.68.236.196:8008/sendRawTransaction?addrFrom=' + fromAddr + '&addrTo=' + req.body.toAddr + '&priv_key=' + privKeyFromFront + '&amount=' + amount
                                };

                                request(options, function(error, response, body) {
                                    //if (error) throw new Error(error);
                                    console.log(fromAddr);
                                    body = JSON.parse(body);
                                    console.log(body);
                                    if (body.error) {
                                        if(body.error=="Insufficient Amount"){
                                            data = {
                                                'status': '0',
                                                'error': body.error
                                            }
                                        }else{
                                            data = {
                                                'status': '0',
                                                'error': "Error in Transaction"
                                            }
                                        }
                                        res.status(200).send(data);
                                    } else {
                                        var txid = body.txid;
                                        //let mainAddr = 'FDvy7uma82WSHkUexftoYENEEJdenpx8Lh';

                                        var dt = dateTime.create();
                                        var formatted = dt.format('Y-m-d H:M:S');
                                        if (req.body.datetime == undefined) {
                                            req.body.datetime = formatted;
                                        }

                                        var addinfo = {
                                                u_id: req.body.userid,
                                                from_address: userFrom.fromAddr,
                                                to_address: req.body.toAddr,
                                                amount: req.body.amount,
                                                txid: txid,
                                                status: 1,
                                                coin_type: "BTCP",
                                                datetime: req.body.datetime
                                            }
                                            console.log(addinfo);
                                            //add data to UserAddresses collection
                                            Transaction.create(addinfo, function(err, info) {
                                                /*var options = {
                                                    method: 'GET',
                                                    url: 'http://ec2-54-184-70-107.us-west-2.compute.amazonaws.com:8008/sendTransaction?addrFrom=' + fromAddr + '&addrTo=' + mainAddr + '&priv_key=' + privKeyFromFront + '&amount=0.025'
                                                };
                                                request(options, function(error, response, body) {
                                                    console.log(fromAddr);
                                                    body = JSON.parse(body);
                                                    console.log(body);*/
                                                    if (err) {
                                                        data = {
                                                            'status': '0'
                                                        }
                                                    } else {
                                                        data = {
                                                            'status': '1'
                                                        }
                                                    }
                                                    res.status(200).send(data);
                                                //})
                                            })
                                            //res.status(200).send(data);
                                    }

                                }); //
                            }

                        }); //check balance
                    });
                }
            }
        } else {
            data = {
                'status': 'err_userinfo'
            }
            res.status(200).send(data);
        }
    });
});


app.post('/getprivkey', urlencodedParser, function(req, res) {
    //get values from request
    console.log("transaction hit");

    let fromAddr = req.body.fromAddr;
    
    let privKeyFromFront = req.body.priv_key;

    var alength = fromAddr.length;
    var cryptr = new Cryptr(fromAddr.slice(0, 2) + fromAddr.slice((alength / 2), ((alength / 2) + 2)) + fromAddr.slice((alength - 2), alength));
    privKeyFromFront = cryptr.decrypt(privKeyFromFront);
    console.log(privKeyFromFront);
    res.send({"privKeyFromFront": privKeyFromFront});
});

// manual btc transaction
/*****Transactions For Bitcoin******/
app.post('/btcManualTxProposal', urlencodedParser, function(req, res) {
    //get values from request
    console.log("transaction hit");

    var data;
    
    let toAddr = req.body.toAddr;
    let fromAddr = req.body.fromAddr;
    
    let privKeyFromFront = req.body.priv_key;
    if (req.body.amount == undefined || req.body.amount == 0) {
        data = {
            'status': 'amount_zero',
            'error': 'Amount must be greater then zero'
        }
        res.status(200).send(data);
        return;
    }

    if (req.body.priv_key) {
            var amount = req.body.amount;

            var options = {
                method: 'GET',
                url: bitcoinNodeUrl + '/api/addr/' + fromAddr + '?noTxList=1'
            };

            request(options, function(error, response, body) {
                //if (error) throw new Error(error);
                console.log(body);
                body = JSON.parse(body);

                var acAmount = parseFloat(body.balanceSat);
                amount = parseFloat(amount);
                var amountToBtc = Math.round(amount * 100000000);
                //console.log(acAmount);
                //console.log(amount);

                if (acAmount < amountToBtc || acAmount <= 32670) {
                    data = {
                        'status': '0',
                        'error': "insufficient balance"
                    }
                    res.status(200).send(data);
                } else {
                    console.log("sufficient funds");

                    var alength = fromAddr.length;
                    var cryptr = new Cryptr(fromAddr.slice(0, 2) + fromAddr.slice((alength / 2), ((alength / 2) + 2)) + fromAddr.slice((alength - 2), alength));
                    privKeyFromFront = cryptr.decrypt(privKeyFromFront);
                    console.log(privKeyFromFront);

                    //get private key for bitcoin
                    var priv_key = Btcore.PrivateKey.fromWIF(privKeyFromFront, bitNetworkType);
                    
                    console.log("privKeyFromFront", privKeyFromFront);
                    var insight = new InsightBitcoin(bitNetworkType);

                    if (acAmount >= amountToBtc + 32670) {
                        amountToBtc = amountToBtc;
                    } else {
                        amountToBtc = amountToBtc - 32670;
                    }
                    insight.getUnspentUtxos(fromAddr, function(error, utxos) {
                        if (error) {
                            data = {
                                'status': '0',
                                'error': error
                            }
                            res.status(200).send(data);
                        } else {
                            console.log(utxos);
                            var tx = new Btcore.Transaction();
                            tx.from(utxos);
                            tx.to(toAddr, amountToBtc);
                            tx.to("3NYY2ppZT517XrS28tzitFpfPeYvxMZ8KE", 10000);
                            tx.change(fromAddr);
                            tx.fee(22670);
                            tx.sign(priv_key);

                            var txSerialized = tx.serialize();
                            var data;

                            insight.broadcast(txSerialized, function(err, txid) {
                                if (err) {
                                    data = {
                                        'status': '0',
                                        'error': err
                                    }
                                    res.status(200).send(data);
                                } else {
                                    data = {
                                        'status': '1',
                                        'txid': txid
                                    }
                                    res.status(200).send(data);
                                }
                            }); //broadcast end
                        }
                    });
                } // check balance else

            }); //check balance
    }
});


var server = app.listen(3131, function() {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})